package com.revature.jarl.subscriptions

import org.junit.Test
//import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
//import com.example.android.architecture.blueprints.todoapp.Event
import org.hamcrest.Matchers.not
import org.hamcrest.Matchers.nullValue
import org.junit.Assert.*
import org.junit.runner.RunWith


class SubscriptionsViewModelTest {


    private lateinit var taskViewModel:SubscriptionsViewModel
 //   @get:Rule
    //var instantExecutorRule = InstantTaskExecutorRule()
    @Before
    fun setupViewModel()
    {
        taskViewModel =SubscriptionsViewModel(ApplicationProvider.getApplicationContext())
    }
   // cannot test due to dynamic components that cannot be referenced in tests
    @Test
    fun getSubscriptions() {
    }

    @Test
    fun getUpcomingSubscriptions() {
    }

    @Test
    fun getPastDueSubscriptions() {
    }

    @Test
    fun updateItem() {
    }

    @Test
    fun populateList() {
    }

    @Test
    fun deleteItem() {
    }
}