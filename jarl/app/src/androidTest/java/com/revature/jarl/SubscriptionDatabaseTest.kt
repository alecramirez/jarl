package com.revature.jarl

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.revature.jarl.repository.database.SubscriptionDatabase
import com.revature.jarl.repository.database.SubscriptionDatabaseDao
import com.revature.jarl.repository.database.UserSubscription
import org.junit.After
import org.junit.Test
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.runBlocking
import org.joda.time.LocalDate
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import java.io.IOException
import java.lang.Exception
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class SubscriptionDatabaseTest {
    private lateinit var subDao : SubscriptionDatabaseDao
    private lateinit var db : SubscriptionDatabase

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun createDb(){
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, SubscriptionDatabase::class.java).build()

        subDao = db.subscriptionDatabaseDao

    }

    @After
    @Throws(IOException::class)
    fun closeDb(){
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeSubscriptionAndGetNextSub() = runBlocking{
        val userSubscription = UserSubscription().apply {
            providerName = "Netflix"
            subscriptionId = 1
        }
        subDao.insert(userSubscription)

        val nextSub = subDao.getTimeUntilNextSubscription(LocalDate.now())
        assertEquals(nextSub, userSubscription)
    }

    @Test
    fun insertAndGetAllSubs() = runBlocking{
        val userSubscription1 = UserSubscription().apply {
            subscriptionId = 1
        }

        val userSubscription2 = UserSubscription().apply {
            subscriptionId = 2
        }

        subDao.insert(userSubscription1)
        subDao.insert(userSubscription2)

        val subs = subDao.getAllSubscriptions().blockingObserve()
        assertEquals(subs?.get(0), userSubscription1)
    }

    @Test
    fun insertAndGetPastSubs() = runBlocking{
        val userSubscription1 = UserSubscription().apply {
            subscriptionId = 1
        }

        val userSubscription2 = UserSubscription().apply {
            subscriptionId = 2
            dueDate = LocalDate.now().minusDays(2)
        }

        subDao.insert(userSubscription1)
        subDao.insert(userSubscription2)

        val subs = subDao.getPastSubscriptions(LocalDate.now().minusDays(1)).blockingObserve()
        assertEquals(subs?.get(0), userSubscription2)
    }

    @Test
    fun testInsertAndUpdate() = runBlocking{
        val userSubscription = UserSubscription().apply {
            subscriptionId = 1
        }

        subDao.insert(userSubscription)
        //update the subscription name
        userSubscription.providerName = "test"

        //we should update because it already exists in the db
        subDao.insertOrUpdateSubscription(userSubscription)

        //this should be the same sub as the one we made prior
        val updatedSub = subDao.getSubscriptionById(userSubscription.subscriptionId)

        assertEquals(updatedSub, userSubscription)
    }

}


private fun <T> LiveData<T>.blockingObserve(): T? {
    var value: T? = null
    val latch = CountDownLatch(1)

    val observer = Observer<T> { t ->
        value = t
        latch.countDown()
    }

    observeForever(observer)

    latch.await(2, TimeUnit.SECONDS)
    return value
}