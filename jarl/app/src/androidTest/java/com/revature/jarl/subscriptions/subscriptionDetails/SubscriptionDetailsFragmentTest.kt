package com.revature.jarl.subscriptions.subscriptionDetails

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.revature.jarl.R
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

import org.joda.time.LocalDate

@RunWith(AndroidJUnit4::class)
class SubscriptionDetailsFragmentTest{


    @Test fun testSubDetailsFragmentProviderNameIsInvisible(){
        val fragmentArgs = bundleOf("subscriptionId" to -1)
        launchFragmentInContainer<SubscriptionDetailsFragment>(fragmentArgs,R.style.Theme_JARL)

        onView(withId(R.id.providerLabel)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)))

    }

    @Test fun testSubDetailsFragmentProviderNameIsVisible(){
        val fragmentArgs = bundleOf("subscriptionId" to 2)
        launchFragmentInContainer<SubscriptionDetailsFragment>(fragmentArgs,R.style.Theme_JARL)

        onView(withId(R.id.providerLabel)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))
    }

    @Test
    fun testNewSubscriptionAmount(){
        val fragmentArgs = bundleOf("subscriptionId" to -1)
        launchFragmentInContainer<SubscriptionDetailsFragment>(fragmentArgs,R.style.Theme_JARL)

        onView(withId(R.id.amountEditText)).check(matches(withText("")))

    }

    @Test
    fun testExistingSpectrumSubscription(){
        val fragmentArgs = bundleOf("subscriptionId" to 2)
        launchFragmentInContainer<SubscriptionDetailsFragment>(fragmentArgs,R.style.Theme_JARL)

        onView(withId(R.id.amountEditText)).check(matches(withText("89.99")))
    }


    @Test
    fun testNewSubscriptionDate(){
        val fragmentArgs = bundleOf("subscriptionId" to -1)
        launchFragmentInContainer<SubscriptionDetailsFragment>(fragmentArgs,R.style.Theme_JARL)

        //makes sure it is displayed
        onView(withId(R.id.dateTextView)).check(matches(isDisplayed()))

        //checks that the date is correct
        onView(withId(R.id.dateTextView)).check(matches(withText(LocalDate.now().toString("EEE M/d"))))
    }


}


