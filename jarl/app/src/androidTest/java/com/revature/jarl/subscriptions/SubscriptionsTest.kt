package com.revature.jarl.subscriptions

import androidx.test.core.app.ActivityScenario
import org.junit.After
import org.junit.Before

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.revature.jarl.MainActivity
import com.revature.jarl.R
import org.junit.Assert.*
import org.junit.Test

class SubscriptionsTest {


    @Before
    fun setUp() {

        ActivityScenario.launch(MainActivity::class.java)
        onView(withText("Get Started!")).check(matches(isDisplayed()))
        onView(withId(R.id.btn_alec_button)).perform(click())
    }

    @Test
    fun testTabsWorking()
    {

        onView(withText("Upcoming")).check(matches(isDisplayed()))
        onView(withText("All Subscriptions")).check(matches(isDisplayed()))
//        onView(withText("Past Due")).check(matches(isDisplayed()))
        onView(withId(R.id.subscriptions_pager)).perform(click())
        onView(withId(R.id.add_subscriptions_floating_btn)).perform(click())
    }

    @After
    fun tearDown() {

    }
}