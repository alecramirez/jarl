package com.revature.jarl.repository

import com.revature.jarl.repository.database.UserSubscription
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Test

import org.junit.Assert.*

class SubscriptionRepositoryTest {

    @Test
    fun insertOrUpdateSubscription() {
        GlobalScope.launch {
            insertOrUpdateSubscription()
        }
    }

    @Test
    fun getAllSubscriptions() {
    }

    @Test
    fun getSubscriptionById() {
    }

    @Test
    fun clearSubscriptions() {
    }

    @Test
    fun getPastSubscriptions() {
    }

    @Test
    fun getUpComingSubscriptions() {
    }

    @Test
    fun getSubscriptionsByDay() {
    }

    @Test
    fun getPastAndPresentSubscriptions() {
    }
}