package com.revature.jarl.subscriptions

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.revature.jarl.subscriptions.subscriptionDetails.SubscriptionDetailsViewModel
import java.lang.IllegalArgumentException

class SubscriptionViewModelFactory(private val context:Context):ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(SubscriptionsViewModel::class.java)){
//            return SubscriptionsViewModel() as T
//        }
        throw IllegalArgumentException("UnknownViewModelCast")
    }
}