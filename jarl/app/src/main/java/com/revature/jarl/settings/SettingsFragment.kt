package com.revature.jarl.settings

import android.app.Activity
import android.content.*
import android.graphics.*
import android.graphics.ImageDecoder.*
import android.os.*
import android.provider.MediaStore
import android.text.InputType.*
import android.util.Log
import android.view.*
import android.view.KeyEvent.*
import android.widget.*
import androidx.fragment.app.*
import com.revature.jarl.*
import com.revature.jarl.databinding.FragmentSettingsBinding
import java.io.*


class SettingsFragment : Fragment() {

    private val GET_FROM_GALLERY = 1

    private val activity by lazy { requireActivity() as MainActivity }
    private val vm: SettingsViewModel by viewModels()
    private lateinit var binding: FragmentSettingsBinding
    private val outputDirectory
        get() = activity.externalMediaDirs.firstOrNull()?.apply {
            mkdirs()
        } ?: activity.filesDir

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentSettingsBinding.inflate(inflater).run{

        binding = this

        lifecycleOwner = this@SettingsFragment
        viewModel = vm

        tvDays.setAdapter( ArrayAdapter(activity, R.layout.day_item, (0..14).toList()))

        fabEdit.setOnClickListener { editInfo() }
        swAutopay.setOnCheckedChangeListener { _, isChecked -> vm.autopay = isChecked }
        shivUser.setOnClickListener { selectImage() }
        etName.setOnKeyListener(NameKeyListener())
        etPhone.setOnKeyListener(NameKeyListener())

        tvDays.setOnItemClickListener { _, _, position, _ ->
            vm.daysBefore = position
            clearFocus()
        }

        root
    }

    private fun selectImage() {
        startActivityForResult(
            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI),
            GET_FROM_GALLERY
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val uri = data?.data
        if(requestCode != GET_FROM_GALLERY || resultCode != Activity.RESULT_OK || uri == null) return

        val bmp = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            decodeBitmap(createSource(activity.contentResolver, uri))
        else
            MediaStore.Images.Media.getBitmap(activity.contentResolver, uri)

        saveImage(bmp)
        bindImage(binding.shivUser, vm.image)
    }

    private fun editInfo() {
        binding.tilName.apply {
            isEnabled = !isEnabled
            editText?.let { it.inputType = if (isEnabled) 8193 else 139265 }
        }
        binding.etPhone.apply { isEnabled = !isEnabled }
        binding.etName.saveInfo()
        binding.etPhone.saveInfo()
    }

    private fun EditText.saveInfo(): Boolean {
        this@SettingsFragment.clearFocus()

        when (id) {
            R.id.et_name ->
                vm.name = text.toString()
            R.id.et_phone ->
                if (text.length == 14)
                    vm.phone = text.toString()
                else
                    error = "invalid phone"
        }
        return false
    }

    private fun saveImage(image: Bitmap) {

        FileOutputStream(
            File(outputDirectory, getString(R.string.profile_image_name))
        ).apply {
            image.compress(Bitmap.CompressFormat.JPEG, 100, this)
        }.close()
    }

    private fun clearFocus() = activity.findViewById<View>(R.id.root).callOnClick()

    inner class NameKeyListener: View.OnKeyListener {

        override fun onKey(v: View?, keyCode: Int, event: KeyEvent?) =
            if (!listOf(KEYCODE_ENTER, KEYCODE_NUMPAD_ENTER).contains(keyCode))
                false
            else (v as EditText).saveInfo()
    }
}
