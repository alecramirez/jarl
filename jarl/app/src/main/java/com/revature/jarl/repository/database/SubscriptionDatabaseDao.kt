package com.revature.jarl.repository.database

import androidx.lifecycle.LiveData
import androidx.room.*
import org.joda.time.LocalDate
import java.lang.Exception


/**
 * This is the DAO that defines the methods in which we can access our Subscription Database.
 *
 * @author Jacob Ginn
 */
@Dao
interface SubscriptionDatabaseDao {



    @Insert
    suspend fun insert(subscription : UserSubscription)

    @Update
    suspend fun update(subscription : UserSubscription)

    /**
     * gets a single subscription based on its ID.
     */
    @Query("SELECT * FROM subscriptions_table WHERE subscription_id = :id")
    suspend fun getSubscriptionById(id : Int) : UserSubscription?

    /**
     * gets all of the subscriptions and orders them based on the date of the next payment.
     */
    @Query("SELECT * FROM subscriptions_table ORDER BY date(due_date) ")
    fun getAllSubscriptions() : LiveData<List<UserSubscription>>

    /**
     * Clears all of the rows from a database table
     */
    @Query("DELETE FROM subscriptions_table")
    suspend fun clear()

    @Delete
    suspend fun delete(subscription: UserSubscription)

    /**
     * This function determines if a subscription should be inserted or updated based on if it
     * exists in the database.
     */
    suspend fun insertOrUpdateSubscription(subscription: UserSubscription){
        val userSubscription = getSubscriptionById(subscription.subscriptionId)

        if (userSubscription == null){
            insert(subscription)
        }else{
            update(subscription)
        }
    }

    /**
     * Gets all of the subscriptions that were due before today
     */
    @Query("SELECT * FROM subscriptions_table WHERE date(due_date)<date(:date)  ORDER BY date(due_date) ")
    fun getPastSubscriptions(date:LocalDate) : LiveData<List<UserSubscription>>

    /**
     * Gets all of the subscriptions that were due before the date specified by the future date
     */
    @Query("SELECT * FROM subscriptions_table WHERE date(due_date) BETWEEN date(:date) AND date(:futureDate) ORDER BY date(due_date) ")
    fun getUpcomingSubscriptions( date:LocalDate, futureDate:LocalDate) : LiveData<List<UserSubscription>>

    /**
     * Gets all of the subscriptions that have the date that is specified in date
     */
    @Query("SELECT * FROM subscriptions_table WHERE date(due_date) IS date(:date)")
    fun getDaySubscriptions(date:LocalDate) : LiveData<List<UserSubscription>>

    /**
     * Gets the next subscription that the user should be notified about.
     */
    @Query("SELECT * FROM subscriptions_table WHERE date(due_date) >= date(:now) AND hasNotified ORDER BY due_date LIMIT 1")
    fun getTimeUntilNextSubscription(now : LocalDate) : UserSubscription

}