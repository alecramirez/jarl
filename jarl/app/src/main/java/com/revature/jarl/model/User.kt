package com.revature.jarl.model

data class User(
    val username: String,
    val password: String
)