package com.revature.jarl.subscriptions

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.revature.jarl.repository.SubscriptionRepository
import com.revature.jarl.repository.database.SubscriptionDatabase
import com.revature.jarl.repository.database.UserSubscription
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import org.joda.time.LocalDate

class SubscriptionsViewModel(application: Application) : AndroidViewModel(application) {

    private val dao = SubscriptionDatabase.getInstance(getApplication()).subscriptionDatabaseDao
    private val repo = SubscriptionRepository(dao)

    private lateinit var _subscriptions:LiveData<List<UserSubscription>>
    val subscriptions: LiveData<List<UserSubscription>>
        get() = _subscriptions

    private lateinit var _upcomingSubscriptions:LiveData<List<UserSubscription>>
    val upcomingSubscriptions: LiveData<List<UserSubscription>>
        get() = _upcomingSubscriptions

    private lateinit var _pastDueSubscriptions:LiveData<List<UserSubscription>>
    val pastDueSubscriptions: LiveData<List<UserSubscription>>
        get() = _pastDueSubscriptions


    init {
            populateList()
         }



    fun updateItem()
    {

    }
    fun populateList()
    {

        var x = viewModelScope.launch {

            val dao = SubscriptionDatabase.getInstance(getApplication()).subscriptionDatabaseDao
            val repo = SubscriptionRepository(dao)

            _subscriptions = repo.getAllSubscriptions() as LiveData<List<UserSubscription>>
            _upcomingSubscriptions =
                repo.getUpComingSubscriptions(
                LocalDate.now(),
                LocalDate.now().plusWeeks(1))
            _pastDueSubscriptions = repo.getPastSubscriptions(LocalDate.now())
//            Log.i("Database Accessed", "We now have:\n${_subscriptions.value}")
        //    repo.clearSubscriptions()
        }

    }

    fun deleteItem()
    {

    }
//    fun changeTab(x:Int)
//    {
//        when(x)
//        {
//            0->{
//                var x = viewModelScope.launch {
//                    val dao = SubscriptionDatabase.getInstance(getApplication()).subscriptionDatabaseDao
//                    _upcomingSubscriptions.value = SubscriptionRepository(dao).getAllSubscriptions() as MutableList<UserSubscription>
//                  //  Log.i("Database Accessed", "We now have:\n${_subscriptions.value}")
//                }
//            }
//            1->{
//                var x = viewModelScope.launch {
//                    val dao = SubscriptionDatabase.getInstance(getApplication()).subscriptionDatabaseDao
//                    _subscriptions.value = SubscriptionRepository(dao).getAllSubscriptions() as MutableList<UserSubscription>
//               //     Log.i("Database Accessed", "We now have:\n${_subscriptions.value}")
//                }
//            }
//            else->{
//                var x = viewModelScope.launch {
//                    val dao = SubscriptionDatabase.getInstance(getApplication()).subscriptionDatabaseDao
//                    _subscriptions.value = mutableListOf()//SubscriptionRepository(dao).getAllSubscriptions() as MutableList<UserSubscription>
//                //    Log.i("Database Accessed", "We now have:\n${_subscriptions.value}")
//                }
//            }
//        }
//    }

}