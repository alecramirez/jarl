package com.revature.jarl.subscriptions.subscriptionDetails

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.revature.jarl.model.RecurrencesEnum
import com.revature.jarl.network.JARLApi
import com.revature.jarl.model.Provider
import com.revature.jarl.model.User
import com.revature.jarl.repository.SubscriptionRepository
import com.revature.jarl.repository.database.DatabaseUtils
import com.revature.jarl.repository.database.SubscriptionDatabase
import com.revature.jarl.repository.database.UserSubscription
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.joda.time.LocalDate

/**
 * This is the view model that contains all of the data that is displayed on the subscription
 * details screen.
 *
 * @author Jacob Ginn
 */
class SubscriptionDetailsViewModel(
    private val subscriptionKey: Int,
    context: Context
) : ViewModel() {
    /** The Database Access object that is used in the repository */
    private val dao = SubscriptionDatabase.getInstance(context).subscriptionDatabaseDao

    /** The repository that we access our database from */
    private val repository = SubscriptionRepository(dao)

    /** if the subscription has been edited */
    private var bool = false

    /**  Debug tag for logging in this view model */
    private val TAG = "SubDetViewModel"

    /** this is the subscription that the user is editing or making a new one */
    private val _subscription = MutableLiveData<UserSubscription>()
    val subscription: LiveData<UserSubscription>
        get() = _subscription

    /** The live data that holds all of the default providers */
    private val _providers = MutableLiveData<MutableList<Provider>>()
    val providers: LiveData<MutableList<Provider>>
        get() = _providers

    /**
     * This is the date for the subscription that is being made or edited
     */
    private val _date = MutableLiveData<UserSubscription?>()
    val date : LiveData<UserSubscription?>
        get() = _date


    /**
     * This just initializes the subscription and the default providers that we provide for our
     * users
     */
    init {
        setUpSubscription()
        getDefaultProviders()
    }

    /**
     * gets the default list of providers to display to the user.
     */
    private fun getDefaultProviders() {
        viewModelScope.launch {
            _providers.value = JARLApi.retrofitService.defaultProviders().toMutableList()
        }
    }

    /**
     * updates the subscriptions frequency of payment
     */
    fun updateRecurrence(recurrencesEnum: RecurrencesEnum) {
        subscription.value!!.recurrence = recurrencesEnum.type
        Log.d(TAG, "setting sub to $recurrencesEnum")
    }

    /**
     * updates the subscriptions next payment date
     */
    fun updateDate(date: LocalDate) {
        bool = true
        subscription.value!!.dueDate = date
    }

    /**
     * updates the subscriptions amount due for the subscription
     */
    fun updateValueDue(amount: Double) {
        bool = true
        subscription.value!!.amountDue = amount
    }

    /**
     * updates the subscriptions provider name
     */
    fun updateProvider(providerName: String) {
        bool = true
        subscription.value!!.providerName = providerName
    }

    /**
     * updates the subscriptions provider image.
     */
    fun updateProviderImage(imageUrl: String){
        bool = true
        subscription.value!!.providerImage = imageUrl
    }


    /**
     * This method will verify that all of the information in the subscription is valid by using
     * the utility functions that we have provided.
     */
    fun verifySubscription(): Boolean {

        subscription.value?.let {
            if (DatabaseUtils.verifySubscription(
                    it.dueDate.toString(),
                    it.amountDue.toString(),
                    it.providerName
                )) {
                Log.d(TAG, "sub verified")

                return true
            }

            Log.d(TAG, "sub not verified")
            return false
        }
        Log.d(TAG, "sub is null")
        return false
    }

    /**
     * This method will add or update the subscription to the database
     */
    fun addToDatabase(){
        if (bool){
            GlobalScope.launch {
                repository.insertOrUpdateSubscription(subscription.value!!)
                getTimeDelay()
                println(subscription.value)
            }
        }
    }

    /**
     * This method sets up the live data subscription that the subscription details fragment watches
     * so that the most up to date information is displayed on the screen.
     */
    private fun setUpSubscription(){
        if (subscriptionKey == -1 ){
            _subscription.value  = UserSubscription()
        }else{
            viewModelScope.launch{
                _subscription.value = repository.getSubscriptionById(subscriptionKey)
            }
        }
    }

    /**
     * This method accesses the database to get the next subscription that needs to be scheduled
     */
    private fun getTimeDelay() {
       GlobalScope.launch {
            _date.postValue(repository.getTimeUntilNextSubscription(LocalDate.now()))
        }
    }

    /**
     * this method accesses the database through the repository and deletes the current subscription
     */
    fun removeSubscription(){
        GlobalScope.launch {
            subscription.value?.let { repository.deleteSubscription(it) }
        }
    }

    /**
     * this method updates the subscription's date based on the frequency of the subscription
     */
    fun markPaid()
    {
        GlobalScope.launch {
            subscription.value?.hasNotified = true
            subscription.value?.let{
                when (subscription.value?.recurrence) {
                    "weekly" -> {
                        it.dueDate = it.dueDate.plusWeeks(1)
                    }
                    "bi-weekly" -> {
                        it.dueDate = it.dueDate.plusWeeks(2)!!
                    }
                    "monthly" -> {
                        it.dueDate = it.dueDate.plusMonths(1)
                    }
                    "quarterly" -> {
                        it.dueDate = it.dueDate.plusMonths(3)
                    }
                    "bi-annually" -> {
                        it.dueDate = it.dueDate.plusMonths(6)
                    }
                    "annually" -> {
                        it.dueDate = it.dueDate.plusYears(1)
                    }
                    else -> null
                }
            }

            subscription.value?.let { repository.insertOrUpdateSubscription(subscription.value!!) }
        }
    }

}