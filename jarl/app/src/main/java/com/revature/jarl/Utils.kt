package com.revature.jarl

import com.revature.jarl.model.RecurrencesEnum

fun convertStringToEnum(type : String) : RecurrencesEnum{
    return when(type){
        "weekly" -> RecurrencesEnum.WEEKLY
        "bi-weekly" -> RecurrencesEnum.BIWEEKLY
        "monthly" -> RecurrencesEnum.MONTHLY
        "quarterly" -> RecurrencesEnum.QUARTERLY
        "bi-annually" -> RecurrencesEnum.BIANNUALLY
        "annually" -> RecurrencesEnum.ANNUALLY
        else -> RecurrencesEnum.MONTHLY
    }
}

