package com.revature.jarl.calendar

import android.app.Application
import androidx.lifecycle.*
import com.revature.jarl.repository.SubscriptionRepository
import com.revature.jarl.repository.database.*
import org.joda.time.LocalDate

class CalenderViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var _roomSubscriptions: LiveData<List<UserSubscription>>

    private val repo = SubscriptionRepository(
        SubscriptionDatabase.getInstance(getApplication()).subscriptionDatabaseDao
    )

    init{ loadSubscriptions(LocalDate()) }

    fun loadSubscriptions(date: LocalDate) {
        _roomSubscriptions = repo.getSubscriptionsByDay(date)
    }

}