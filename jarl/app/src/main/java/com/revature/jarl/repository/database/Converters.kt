package com.revature.jarl.repository.database

import androidx.room.TypeConverter
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

object Converters {

    /** The format for the LocalDate that we are using */
    private const val DATE_FORMAT = "yyyy-MM-dd"

    //This is the converter the database uses to turn a string into a LocalDate
    @TypeConverter
    fun toLocalDate(value : String?): LocalDate? {
        return LocalDate.parse(value, DateTimeFormat.forPattern(DATE_FORMAT))
    }
    //This is the converter the database uses to turn a LocalDate into a String
    @TypeConverter
    fun fromLocalDate(date: LocalDate) : String{
        return date.toString(DATE_FORMAT)

    }
}