package com.revature.jarl

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.revature.jarl.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(){

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_subscriptions,
            R.id.navigation_calendar,
            R.id.navigation_settings,
            R.id.loginFragment
        ))

        NavigationUI.setupActionBarWithNavController(
            this,
            navController,
            appBarConfiguration
        )

        NavigationUI.setupWithNavController(binding.navBar, navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            // TODO this is where fragments go to remove their nav bars - just add the id to the list
            val ids = listOf(
                R.id.loginFragment,

            )
            binding.navBar.visibility = if (ids.contains(destination.id)) GONE else VISIBLE
        }

        // this allows the user to deselect TextViews and hide the soft keyboard by clicking
        // an empty part of screen
        binding.root.setOnClickListener { clearFocusAndHideKeyBoard() }

    }

    override fun onSupportNavigateUp(): Boolean =
        findNavController(R.id.nav_host_fragment).navigateUp() || super.onSupportNavigateUp()

    // This clears focus and hides the keyboard
    fun clearFocusAndHideKeyBoard(view: View = binding.root) {
        view.findFocus()?.clearFocus()
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view.windowToken, 0)
        binding.root.clearFocus()
    }

    fun getLayoutListener(view: View) = ViewHider(view)

    open inner class ViewHider(val view: View): ViewTreeObserver.OnGlobalLayoutListener {

        var isKeyBoardVisible = false

        open fun onKeyboardVisibilityChanged() {
            view.visibility = if ( isKeyBoardVisible ) GONE else VISIBLE

            findViewById<FloatingActionButton>(R.id.subDetailsFAB)?.updateLayoutParams<ConstraintLayout.LayoutParams> {
                bottomToBottom = if(isKeyBoardVisible) -1 else R.id.providerLogo
                topToBottom = if(isKeyBoardVisible) R.id.dateTextView else -1
            }
        }

        override fun onGlobalLayout() {

            if ( isKeyBoardVisible.run {
                    isKeyBoardVisible = binding.root.let {
                        (it.rootView.height - it.height) / resources.displayMetrics.density > 200
                    }
                    isKeyBoardVisible != this
            })
                onKeyboardVisibilityChanged()
        }
    }

}


/*
Payment reminder app to send user notifications for subscriptions and other recurring payments.
 */