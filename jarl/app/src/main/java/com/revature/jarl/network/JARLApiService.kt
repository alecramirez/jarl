package com.revature.jarl.network

import com.revature.jarl.model.Provider
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "https://private-c2d541-jarl1.apiary-mock.com/"

private val moshi = MoshiConverterFactory.create(Moshi.Builder().add(KotlinJsonAdapterFactory()).build())
private val retrofit = Retrofit.Builder().addConverterFactory(moshi).baseUrl(BASE_URL).build()


interface JARLApiService {

    @GET("providers")
    suspend fun defaultProviders(): List<Provider>

}

object JARLApi {

    val retrofitService: JARLApiService by lazy {
        retrofit.create(JARLApiService::class.java)
    }

}