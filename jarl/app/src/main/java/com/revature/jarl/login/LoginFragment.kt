package com.revature.jarl.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.revature.jarl.R
import com.revature.jarl.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentLoginBinding.inflate(inflater)

        binding.btnAlecButton.setOnClickListener { skipLogin() }

        return binding.root
    }

    private fun skipLogin() {
        try {
            findNavController().navigate(R.id.action_loginFragment_to_subscriptions)
        } catch (e: Exception) {
            Log.i(tag, "failed to skip login - trying again...")
            startActivity(requireActivity().intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        }
    }
}