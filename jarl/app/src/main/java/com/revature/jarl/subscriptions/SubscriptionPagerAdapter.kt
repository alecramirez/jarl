package com.revature.jarl.subscriptions

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProvider

class SubscriptionPagerAdapter(val context: Context, fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() =
        if(context.getSharedPreferences("preferences", Context.MODE_PRIVATE)
                .getBoolean("autopay", true)) 2
        else 3

    override fun getItem(i: Int): Fragment {
  //      var factory = SubscriptionViewModelFactory(requireNotNull(this.activity).application)
 //       fragmentViewModel = ViewModelProvider(this, factory).get(SubscriptionsViewModel::class.java)
        val fragment = AllSubscriptionsFragment()
        fragment.arguments = Bundle().apply {
            // Our object is just an integer :-P
            putInt(ARG_OBJECT, i )
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence {

        return when(position)
        {
            0-> "Upcoming"
            1->"All Subscriptions"
            else-> "Overdue"
        }
    }
}

private const val ARG_OBJECT = "SUBSCRIPTIONS"