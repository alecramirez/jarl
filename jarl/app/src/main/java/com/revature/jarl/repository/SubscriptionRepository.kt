package com.revature.jarl.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.revature.jarl.repository.database.SubscriptionDatabaseDao
import com.revature.jarl.repository.database.UserSubscription
import org.joda.time.LocalDate

class SubscriptionRepository(private val subscriptionDao : SubscriptionDatabaseDao) {

    /**
     * This function determines if a subscription should be inserted or updated based on if it
     * exists in the database.
     */
    suspend fun insertOrUpdateSubscription(subscription: UserSubscription) =
        subscriptionDao.insertOrUpdateSubscription(subscription)

    /**
     * gets all of the subscriptions and orders them based on the date of the next payment.
     */
    fun getAllSubscriptions() = subscriptionDao.getAllSubscriptions()


    /**
     * gets a single subscription based on its ID.
     */
    suspend fun getSubscriptionById(id: Int) = subscriptionDao.getSubscriptionById(id)


    /**
     * Clears all of the rows from a database table
     */
    suspend fun clearSubscriptions() = subscriptionDao.clear()

    /**
     * Deletes a subscription
     */
    suspend fun deleteSubscription(subscription: UserSubscription) =
        subscriptionDao.delete(subscription)


    /**
     * Gets all of the subscriptions that were due before today
     */
    fun getPastSubscriptions(date: LocalDate) = subscriptionDao.getPastSubscriptions(date)

    /**
     * Gets all of the subscriptions that were due before the date specified by the future date
     */
    fun getUpComingSubscriptions(date: LocalDate, futureDate: LocalDate) =
        subscriptionDao.getUpcomingSubscriptions(date, futureDate)


    /**
     * Gets all of the subscriptions that were due before the date specified by the future date
     */
    fun getSubscriptionsByDay(date: LocalDate) = getUpComingSubscriptions(date, date)


    /**
     * Gets the next subscription that the user should be notified about.
     */
    fun getTimeUntilNextSubscription(now : LocalDate) = subscriptionDao.getTimeUntilNextSubscription(now)
}