package com.revature.jarl.repository.database

import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import java.lang.Exception
import java.lang.NumberFormatException

/**
 * This is a utilities class that is used to check the input of the user before we insert a
 * subscription into the database.
 *
 * @author Jacob Ginn
 */
class DatabaseUtils {


    companion object {
        /** The format for the LocalDate that we are using */
        private const val DATE_FORMAT = "yyyy-MM-dd"

        /**
         * This function checks all of the users inputs and returns true if it finds no errors
         * and false if it finds errors in the users inputs.
         *
         * @param date - the date the user input
         * @param amount - the amount the subscription is worth
         * @param providerName - the name of the subscription provider
         */
        fun verifySubscription(
            date: String,
            amount: String,
            providerName: String
        ): Boolean {

            var result = true
            
            try {
                if (amount.isEmpty() || amount.toDouble() <= 0)
                    result = false
            } catch (n: NumberFormatException) {
                result = false
            }


            if (providerName.isEmpty())
                result = false

            try {
                LocalDate.parse(
                    date,
                    DateTimeFormat.forPattern(DATE_FORMAT)
                )
            } catch (e: Exception) {
                result = false
            }

            return result
        }
    }
}