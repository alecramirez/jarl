package com.revature.jarl.subscriptions

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.revature.jarl.R

class Subscriptions : Fragment() {
    private lateinit var subscriptionPagerAdapter: SubscriptionPagerAdapter
    private lateinit var viewPager: ViewPager
    private lateinit var root:View

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.subscriptions_fragment, container, false)

        root.findViewById<FloatingActionButton>(R.id.add_subscriptions_floating_btn).setOnClickListener {
            findNavController().navigate(R.id.action_subscriptions_to_subscriptionDetailsFragment)
            Log.i("Subscription", "New Subscription Added")
      //      TestData.addSubs(Subscription("Testing"))
        }
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        subscriptionPagerAdapter = SubscriptionPagerAdapter(requireContext(), childFragmentManager)
        viewPager = view.findViewById(R.id.subscriptions_pager)
        viewPager.adapter = subscriptionPagerAdapter
    }
}


