package com.revature.jarl.subscriptions.subscriptionDetails

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.revature.jarl.repository.database.SubscriptionDatabaseDao
import java.lang.IllegalArgumentException

class SubscriptionDetailsViewModelFactory(
        private val subscriptionKey : Int,
        private val context: Context
) : ViewModelProvider.Factory {


    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SubscriptionDetailsViewModel::class.java)){
            return SubscriptionDetailsViewModel(subscriptionKey,context) as T
        }
        throw IllegalArgumentException("UnknownViewModelCast")
    }

}