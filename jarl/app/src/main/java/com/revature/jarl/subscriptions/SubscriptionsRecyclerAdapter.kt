package com.revature.jarl.subscriptions

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.revature.jarl.databinding.SubscriptionItemBinding
import com.revature.jarl.repository.database.UserSubscription

class SubscriptionsRecyclerAdapter(val listener:CustomSubscriptionClickListener)
    :ListAdapter<UserSubscription, SubscriptionsRecyclerAdapter.SubscriptionViewHolder>(DiffCallback){

    companion object DiffCallback : DiffUtil.ItemCallback<UserSubscription>() {
        override fun areItemsTheSame(oldItem: UserSubscription, newItem: UserSubscription): Boolean {
            return oldItem === newItem
        }
        override fun areContentsTheSame(oldItem: UserSubscription, newItem: UserSubscription): Boolean {
            return oldItem.subscriptionId == newItem.subscriptionId
        }
    }

    class SubscriptionViewHolder(val binding:SubscriptionItemBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(sub:UserSubscription)
        {
            binding.subscription = sub
        }
    }

    //This is where we specify what items to "inflate" as cards in the recycler view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscriptionViewHolder {
        return SubscriptionViewHolder(SubscriptionItemBinding.inflate(LayoutInflater.from(parent.context)))

    }

    override fun onBindViewHolder(holder: SubscriptionViewHolder, position: Int) {
        val subscription = getItem(position)
        holder.binding.root.layoutParams = ConstraintLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            .apply { setMargins(8,16,8,16) }
        holder.itemView.setOnClickListener {
            listener.onClick(subscription)
        }

        holder.bind(subscription)
    }

    override fun submitList(list: List<UserSubscription>?) {
        super.submitList(list)
    }
    class CustomSubscriptionClickListener(val clickListener : (UserSubscription) -> Unit){
        fun onClick(subscription: UserSubscription) = clickListener(subscription)
    }
}