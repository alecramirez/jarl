package com.revature.jarl.subscriptions

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.revature.jarl.bindSubscriptionRecycler
import com.revature.jarl.databinding.AllSubscriptionsFragmentBinding

class AllSubscriptionsFragment : Fragment() {

    private lateinit var binding: AllSubscriptionsFragmentBinding
    private lateinit var recyclerView: RecyclerView
    private val viewModel: SubscriptionsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = AllSubscriptionsFragmentBinding.inflate(layoutInflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        val subscriptionClick = SubscriptionsRecyclerAdapter.CustomSubscriptionClickListener{subscription->
            val action = SubscriptionsDirections.actionSubscriptionsToSubscriptionDetailsFragment(subscription.subscriptionId)
            findNavController().navigate(action)
        }

        recyclerView = binding.subscriptionRecyclerView
        recyclerView.adapter = SubscriptionsRecyclerAdapter(subscriptionClick)

        viewModel.subscriptions.observe(viewLifecycleOwner,{ update() })
        viewModel.pastDueSubscriptions.observe(viewLifecycleOwner,{ update() })
        viewModel.upcomingSubscriptions.observe(viewLifecycleOwner,{ update() })

        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        update()
    }

    private fun update()
    {
        arguments?.takeIf { it.containsKey("SUBSCRIPTIONS") }?.apply {
            when(getInt("SUBSCRIPTIONS"))
            {
                0-> bindSubscriptionRecycler(recyclerView, viewModel.upcomingSubscriptions.value)
                1-> bindSubscriptionRecycler(recyclerView, viewModel.subscriptions.value)
                else-> bindSubscriptionRecycler(recyclerView, viewModel.pastDueSubscriptions.value)
            }
        }
    }
}