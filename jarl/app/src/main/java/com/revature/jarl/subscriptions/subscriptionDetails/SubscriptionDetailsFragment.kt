package com.revature.jarl.subscriptions.subscriptionDetails

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.*
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.work.*
import com.google.android.material.datepicker.MaterialDatePicker
import com.revature.jarl.*
import com.revature.jarl.R
import com.revature.jarl.databinding.SubscriptionDetailsFragmentBinding
import com.revature.jarl.model.*
import com.revature.jarl.notifications.NotificationWorker
import com.revature.jarl.repository.database.UserSubscription
import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.LocalDate
import org.joda.time.format.*
import java.io.File
import java.io.FileOutputStream
import java.lang.NumberFormatException
import java.util.*
import java.util.concurrent.TimeUnit
import java.time.Duration

/**
 * This is the subscription details fragment that is displayed when the user clicks on a
 * subscription or tries to make a new subscription.
 *
 * @author Jacob Ginn
 */
class SubscriptionDetailsFragment : Fragment() {

    /** the constant to get the picture from the gallery */
    private val GET_FROM_GALLERY = 1

    /** the debug tag for the fragment*/
    private val TAG = "SubDetailsFrag"

    /** the data binding for the fragment that holds a reference to all of the views */
    private var _binding: SubscriptionDetailsFragmentBinding? = null
    private val binding
        get() = _binding!!

    /** the list of all of the recurrence Enum */
    private val listOfRecurrences = RecurrencesEnum.values()

    private val outputDirectory
        get() = activity.externalMediaDirs.firstOrNull()?.apply {
            mkdirs()
        } ?: activity.filesDir

    /** gets the activity when it is needed */
    private val activity by lazy { requireActivity() as MainActivity }

    /** The view model that the view communicates with to get all of the data for the UI */
    private lateinit var viewModel: SubscriptionDetailsViewModel

    /** The arguments that are passed to this screen */
    private val safeArgs: SubscriptionDetailsFragmentArgs by navArgs()

    /**
     * The fragment that is being created. This returns the root of the view heirarchy of the binding.
     * We programmatically inflate our fragments views by overriding this method.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = SubscriptionDetailsFragmentBinding.inflate(layoutInflater, container, false)

        binding.lifecycleOwner = this


        val application = requireNotNull(this.activity).application
        val viewModelFactory =
            SubscriptionDetailsViewModelFactory(safeArgs.subscriptionId, application)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(SubscriptionDetailsViewModel::class.java)

        //don't ever forget this again
        binding.viewModel = viewModel


        (activity as? MainActivity)?.let {
            binding.root.viewTreeObserver.addOnGlobalLayoutListener(
                it.getLayoutListener(binding.providerLogo)
            )
        }

        return binding.root
    }

    /**
     * All of the views have now been created so we setup all of the listeners and observers of the
     * live data.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupNextDate()
        setupLogo()
        setupDateTextView()
        setupRecurrenceList()
        setupProvidersList()
        setupCustomProvider()
        setupAmount()
        setupFAB()
        setupScrollView()
        if (safeArgs.subscriptionId != -1) {
            binding.customProviderTextField.visibility = View.INVISIBLE
            binding.providerTextField.visibility = View.INVISIBLE
            binding.providerLabel.visibility = View.VISIBLE
            setHasOptionsMenu(true)
        }
    }

    /**
     * The screen has options so here is where we inflate them
     */
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.subscription_details_menu, menu)
    }

    /**
     * This sets up the functionality for when a menu item is clicked.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.remove -> {
                viewModel.removeSubscription()

                findNavController().navigateUp()
            }
            R.id.mark_paid->{
                viewModel.markPaid()
                findNavController().navigateUp()
            }
            else -> false
        }
    }

    /**
     * Sets up the provider logo to change when a provider is changed
     */
    private fun setupLogo() {
        binding.providerLogo.setOnClickListener {
            startActivityForResult(
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                GET_FROM_GALLERY
            )
        }
    }


    /**
     * sets up the floating action button to check for errors and display them to the user.
     */
    private fun setupFAB() {
        binding.subDetailsFAB.setOnClickListener {

            binding.amountEditText.let {
                if (it.text.toString().isEmpty())
                    it.error = "Error: enter amount due"
                else {
                    try {
                        if (it.text.toString().toDouble() <= 0f) {
                            it.error = "Error: value must be positive"
                        }
                    } catch (n: NumberFormatException) {
                        it.error = "Error: value not recognized"
                    }
                }
            }

            binding.dateTextView.let {
                if (it.text.toString().isEmpty())
                    it.error = "Error: need payment date"
            }

            binding.customProviderEditText.let {
                if (it.text.toString().isEmpty())
                    it.error = "Error: enter custom provider"
            }

            if (viewModel.verifySubscription()) {
                viewModel.addToDatabase()
            }
        }
    }

    /**
     * sets up the frequency list so that all of the available frequencies are shown.
     */
    private fun setupRecurrenceList() {

        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.recurrence_list_item,
            listOfRecurrences.map { it.type }
        )
        binding.recurrenceTextView.setAdapter(adapter)


        binding.recurrenceTextView.setOnItemClickListener { _, _, position, _ ->
            Log.d(TAG, listOfRecurrences[position].toString())
            viewModel.updateRecurrence(listOfRecurrences[position])
        }
    }

    /**
     * sets up the custom provider field so that when the user selects it, it will hide the other
     * provider field to make room.
     */
    private fun setupCustomProvider() {
        binding.customProviderEditText.addTextChangedListener {
            binding.providerTextField.visibility = View.GONE
            viewModel.updateProvider(it.toString())
        }
    }

    /**
     * sets up the amount text field so that when the user types an amount it will present the user
     * with an error message if the input is wrong.
     */
    private fun setupAmount() {
        binding.amountEditText.addTextChangedListener {
            if (it.toString().isEmpty())
                binding.amountEditText.error = "Error: enter amount due"
            else {
                try {
                    if (it.toString().toDouble() <= 0f) {
                        binding.amountEditText.error = "Error: value must be positive"
                    } else {
                        binding.amountEditText.error = null
                        viewModel.updateValueDue(it.toString().toDouble())
                    }
                } catch (n: NumberFormatException) {
                    binding.amountEditText.error = "Error: value not recognized"
                }
            }
        }
    }

    /**
     *sets up the providers list so that when one is clicked it will change the amount and the logo
     * to the correct provider.
     */
    private fun setupProvidersList() {

        viewModel.providers.observe(viewLifecycleOwner, { providers ->
            val adapter = ArrayAdapter(
                requireContext(),
                R.layout.recurrence_list_item,
                //R.id.tv_providerName,
                providers
            )
            binding.providerTextView.setAdapter(adapter)
        })

        binding.providerTextView.setOnItemClickListener { parent, _, position, _ ->
            parent.getItemAtPosition(position).apply {
                if (this !is Provider) return@apply

                bindImage(binding.providerLogo, icon)

                binding.customProviderTextField.visibility = View.GONE
                viewModel.updateProvider(name)
                viewModel.updateProviderImage(icon)

                binding.amountEditText.text = SpannableStringBuilder("%.2f".format(amount))
                viewModel.updateValueDue(amount)

                bindAutoCompleteTextView(binding.recurrenceTextView, frequency)
                viewModel.updateRecurrence(convertStringToEnum(frequency))

            }

        }
        binding.providerTextView.onItemSelectedListener
    }

    /**
     * sets up the date picker view so that when the button is clicked it will bring up a date picker
     * for the user to select the next payment date.
     */
    private fun setupDateTextView() {
        fun showDatePicker() {
            val datePicker = MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select Next Payment")
                .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                .build()

            val supportFragmentManager = childFragmentManager
            datePicker.show(supportFragmentManager, "tag")

            datePicker.addOnPositiveButtonClickListener {
                val date = LocalDate(it.plus(86400000))
                binding.dateTextView.text = date.toString()
                viewModel.updateDate(date)
            }
        }

        binding.datePickerButton.setOnClickListener { showDatePicker() }
        binding.dateTextView.setOnClickListener { showDatePicker() }
    }


    /**
     * schedules the notification for however many days ahead it will occur
     */
    private fun scheduleNotification(subscription: UserSubscription) {

        val date = subscription.dueDate.toString()

        //input data for the notification worker
        val inputData: Data = Data.Builder()
            .putInt("years", date.substring(0, date.indexOf('-')).toInt())
            .putInt("months", date.substring(date.indexOf('-') + 1, date.lastIndexOf('-')).toInt())
            .putInt("days", date.substring(date.lastIndexOf('-') + 1, date.length).toInt())
            .putString("provider", subscription.providerName)
            .putInt("subscriptionId", subscription.subscriptionId)
            .build()

        val daysBefore = context?.getSharedPreferences("preferences", Context.MODE_PRIVATE)
            ?.getInt("daysBefore", 3) ?: 0

        val scheduleDay = Days.daysBetween(LocalDate.now(), subscription.dueDate).days - daysBefore

        val myNotification = OneTimeWorkRequestBuilder<NotificationWorker>()
            .setInitialDelay(scheduleDay.toLong(), TimeUnit.DAYS)
            .addTag(date)
            .setInputData(inputData)
            .build()


        WorkManager.getInstance(requireNotNull(this.activity).applicationContext)
//            .enqueue(notificationWorkRequestFuture)
            .enqueueUniqueWork(
                "notifySubscription",
                ExistingWorkPolicy.REPLACE,
                myNotification
            )

        findNavController().navigateUp()
    }

    /**
     *sets up the next date text field so that when a new date is  entered it will reschedule a
     * notification if it is before the other notification.
     */
    private fun setupNextDate() {
        viewModel.date.observe(viewLifecycleOwner, {
            Log.d(TAG, "getting $it")
            if (it != null) {
                scheduleNotification(it)
            }else{
                findNavController().navigateUp()
            }
        })
    }

    /**
     *
     */
    @SuppressLint("ClickableViewAccessibility")
    private fun setupScrollView() {
        binding.detailsScrollView.setOnTouchListener { _, _ ->
            (activity as? MainActivity)?.clearFocusAndHideKeyBoard()
            false
        }
    }

    /**
     *
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val uri = data?.data
        if(requestCode != GET_FROM_GALLERY || resultCode != Activity.RESULT_OK || uri == null) return

        val bmp = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            ImageDecoder.decodeBitmap(ImageDecoder.createSource(activity.contentResolver, uri))
        else
            MediaStore.Images.Media.getBitmap(activity.contentResolver, uri)

        val file = File(outputDirectory, "${DateTime().toString("yyyy_MM_DD_kk_mm_ss")}.jpg")

        FileOutputStream(
            file
        ).apply {
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, this)
        }.close()

        viewModel.updateProviderImage(file.absolutePath)
        bindImage(binding.providerLogo, viewModel.subscription.value?.providerImage)
    }

}
