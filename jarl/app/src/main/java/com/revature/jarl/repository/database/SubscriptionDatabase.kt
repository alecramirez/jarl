package com.revature.jarl.repository.database

import android.content.Context
import androidx.room.*


/**
 * This is the SubscriptionDatabase that defines the entities that are in the database and
 * the version and schema. Only one instance of the database should be created so we put
 * the database creation in a companion object and call the static method of getInstance.
 *
 * @author Jacob Ginn
 */

@Database(entities = [UserSubscription::class], version = 2, exportSchema = false)
@TypeConverters(Converters::class)
abstract class SubscriptionDatabase : RoomDatabase() {

    /**
     * connects the database to the DAO
     */
    abstract val subscriptionDatabaseDao: SubscriptionDatabaseDao

    companion object {

        /** The instance of the database that will be shared across our application */
        @Volatile
        private var INSTANCE: SubscriptionDatabase? = null

        /**
         * This is the getInstance method that returns the instance of the database if one has been
         * made previously or a new instance for the application.
         */
        fun getInstance(context: Context): SubscriptionDatabase {

            // This should not be accessed my multiple things at once
            synchronized(this) {

                //setting the instance of the database
                var instance = INSTANCE

                //if there is no instance we will make one
                if (instance == null) {
                    println("Making Database")
                    instance = Room.databaseBuilder(
                            context.applicationContext,
                            SubscriptionDatabase::class.java,
                        "subscriptions_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}