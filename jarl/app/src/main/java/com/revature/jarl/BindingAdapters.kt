package com.revature.jarl

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageDecoder
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.AutoCompleteTextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.fragment.app.findFragment
import com.google.android.material.textfield.TextInputEditText
import com.revature.jarl.model.RecurrencesEnum
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions.*
//import com.bumptech.glide.signature.*
import com.bumptech.glide.*
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy.*
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.material.button.MaterialButton

import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.textview.MaterialTextView
import com.revature.jarl.repository.database.UserSubscription
import com.revature.jarl.subscriptions.SubscriptionsRecyclerAdapter
import org.joda.time.LocalDate

/**
 * This function will accept a recipe object and set the text in the text view to the
 * appropriate recipe instructions
 */
@BindingAdapter("text_noFilter")
fun bindAutoCompleteTextView(textView: AutoCompleteTextView, input : String?){
    textView.setText(input,false)
}

@BindingAdapter("imageUrl")
fun bindImage(imageView: ShapeableImageView, imageUrl: String?) {
    if (imageUrl == null) return

    val opt = if(imageUrl.contains("http"))
        RequestOptions().placeholder(R.drawable.ic_loading)
    else skipMemoryCacheOf(true).diskCacheStrategy(NONE)

    Glide.with(imageView.context)
        .asBitmap()
        .load(imageUrl)
        .apply(opt)
        .error(R.drawable.ic_dollar)
        .into(imageView)
}

@BindingAdapter("subscriptions")
fun bindSubscriptionRecycler(recyclerView: RecyclerView, subscriptions: List<UserSubscription>?) {
    val adapter = recyclerView.adapter as SubscriptionsRecyclerAdapter
    adapter.submitList(subscriptions)
}

@BindingAdapter("amountDue")
fun bindAmount(textInput: TextInputEditText, amountDue : Double) {
    if (amountDue > 0)
        textInput.setText("%.2f".format(amountDue))
}

@BindingAdapter("date")
fun bindDate(textView: MaterialTextView, date: LocalDate?) {
    textView.text = date?.toString("EEE M/d") ?: ""
}

@BindingAdapter("isPayable")
fun bindPaid(button: MaterialButton, date: LocalDate?) {
    button.visibility = if (date?.isBefore(LocalDate()) == true) View.VISIBLE else View.GONE
}