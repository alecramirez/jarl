package com.revature.jarl.model

data class Provider(
    val name: String,
    val amount: Double,
    val frequency: String,
    val icon: String
)
{
    override fun toString() = name
}
