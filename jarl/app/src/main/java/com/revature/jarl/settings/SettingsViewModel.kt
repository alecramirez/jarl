package com.revature.jarl.settings

import android.app.Application
import android.content.*
import android.util.Log
import androidx.core.content.edit
import androidx.lifecycle.*
import com.revature.jarl.R

class SettingsViewModel(app: Application) : AndroidViewModel(app) {

    private val _sharedPreferences = MutableLiveData<SharedPreferences>()
    private val sharedPreferences: LiveData<SharedPreferences>
        get() = _sharedPreferences

    init {
        _sharedPreferences.value = getApplication<Application>().run {
            getSharedPreferences(getString(R.string.user_info_name), Context.MODE_PRIVATE)
        }
    }

    var name
        get() = sharedPreferences.value?.getString("name", "") ?:""
        set(newName) { _sharedPreferences.put("name", newName) }

    var phone
        get() = sharedPreferences.value?.getString("phone", "") ?:""
        set(newPhone) { _sharedPreferences.put("phone", newPhone) }

    val image by lazy { getApplication<Application>().run {
            "${externalMediaDirs[0]}/${getString(R.string.profile_image_name)}"
        }}

    var autopay
        get() = sharedPreferences.value?.getBoolean("autopay", true) ?:true
        set(newAutopay) { _sharedPreferences.put("autopay", newAutopay) }

    var daysBefore
        get() = sharedPreferences.value?.getInt("daysBefore", 3) ?:3
        set(newDaysBefore) { _sharedPreferences.put("daysBefore", newDaysBefore) }

    private fun MutableLiveData<SharedPreferences>.put(key: String, input: Any) =
        value?.run {
            when (input) {
                is String  -> edit { putString ( key, input) }
                is Int     -> edit { putInt    ( key, input) }
                is Boolean -> edit { putBoolean( key, input) }
                else       -> return false
            }
            postValue(this)
            true
        } ?: false

}