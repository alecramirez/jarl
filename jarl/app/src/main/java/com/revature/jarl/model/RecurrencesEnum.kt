package com.revature.jarl.model

enum class RecurrencesEnum(val type : String) {
    WEEKLY("weekly"),
    BIWEEKLY("bi-weekly"),
    MONTHLY("monthly"),
    QUARTERLY("quarterly"),
    BIANNUALLY("bi-annually"),
    ANNUALLY("annually")

}