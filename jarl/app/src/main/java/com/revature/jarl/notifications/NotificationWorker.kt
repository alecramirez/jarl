package com.revature.jarl.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.NavDeepLinkBuilder
import androidx.work.*
import com.revature.jarl.MainActivity
import com.revature.jarl.R
import com.revature.jarl.model.RecurrencesEnum
import com.revature.jarl.repository.SubscriptionRepository
import com.revature.jarl.repository.database.SubscriptionDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import org.joda.time.Days
import org.joda.time.LocalDate
import java.util.concurrent.TimeUnit

/**
 * This is the NotificationWorker that is made to enqueue to a workmanager so that our application
 * can send notifications at any given time.
 *
 * This is a Coroutine worker because we need to access the database of our application to schedule
 * the next notification.
 *
 * @author Jacob Ginn
 */
class NotificationWorker(
    val context: Context,
    private val workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    companion object {
        private const val CHANNEL_ID = "My Notification"
    }

    /**
     * This function is the function that is called when a worker is executed from a work manager.
     *
     * This function will send the user a notification and schedule the next notification for the
     * next upcoming subscription.
     */
    override suspend fun doWork(): Result {
        createNotificationChannel()

        sendNotification()
        scheduleNotification()

        return Result.success()
    }

    /**
     * This function sets up and sends a notification to the user based on the subscription that
     * is coming up
     */
    private fun sendNotification() {
        //gets the data from the Data object that is given to the worker
        val years = inputData.getInt("years", LocalDate.now().year)
        val months = inputData.getInt("months", LocalDate.now().monthOfYear)
        val days = inputData.getInt("days", LocalDate.now().dayOfMonth)

        val provider = inputData.getString("provider")
        val date = LocalDate(years, months, days)//.minusDays(daysBefore)
        val subId = inputData.getInt("subscriptionId", -1)

        /*
        this is the bundle that contains the sub Id so that we can navigate
        to the correct subscription when they click on the notification
        */

        val bundle = Bundle().apply {
            putInt("subscriptionId", subId)
        }

        //sets the pending intent so that the click takes the user to the details screen
        val pendingIntent  = NavDeepLinkBuilder(context)
            .setGraph(R.navigation.mobile_navigation)
            .setDestination(R.id.subscriptionDetailsFragment)
            .setArguments(bundle)
            .createPendingIntent()

        //builds the notification
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("$provider Subscription Reminder")
            .setContentText("Renewal Date: $date")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .setCategory(NotificationCompat.CATEGORY_MESSAGE)

        //sends the notification
        with(NotificationManagerCompat.from(context)) {
            notify(1, builder.build())
        }
    }

    /**
     * This schedules the next notification when a notification is sent to the user for a
     * subscription
     */
    private suspend fun scheduleNotification() {
        Log.d("NotificationWorker", "Scheduling next notification")
        val dao = SubscriptionDatabase.getInstance(context).subscriptionDatabaseDao
        val repository = SubscriptionRepository(dao)

        val autoPay = context.getSharedPreferences("preferences", Context.MODE_PRIVATE)
            ?.getBoolean("autopay", true) ?: true

        val currentSub = repository.getSubscriptionById(inputData.getInt("subscriptionId", -1))

        if (autoPay) {
            when (currentSub?.recurrence) {
                "weekly" -> {
                    currentSub.dueDate = currentSub.dueDate.plusWeeks(1)
                }
                "bi-weekly" -> {
                    currentSub.dueDate = currentSub.dueDate.plusWeeks(2)
                }
                "monthly" -> {
                    currentSub.dueDate = currentSub.dueDate.plusMonths(1)
                }
                "quarterly" -> {
                    currentSub.dueDate = currentSub.dueDate.plusMonths(3)
                }
                "bi-annually" -> {
                    currentSub.dueDate = currentSub.dueDate.plusMonths(6)
                }
                "annually" -> {
                    currentSub.dueDate = currentSub.dueDate.plusYears(1)
                }
            }
            if (currentSub != null)
                repository.insertOrUpdateSubscription(currentSub)
        }else{
            currentSub?.hasNotified = false
            if (currentSub != null) {
                repository.insertOrUpdateSubscription(currentSub)
            }
        }


        val nextSub = repository.getTimeUntilNextSubscription(LocalDate.now())
        val date = nextSub.dueDate.toString()

        val inputData: Data = Data.Builder()
            .putInt("years", date.substring(0, date.indexOf('-')).toInt())
            .putInt("months", date.substring(date.indexOf('-') + 1, date.lastIndexOf('-')).toInt())
            .putInt("days", date.substring(date.lastIndexOf('-') + 1, date.length).toInt())
            .putString("provider", nextSub.providerName)
            .putInt("subscriptionId", nextSub.subscriptionId)
            .build()

        //gets the users shared preferences for how many days before to be notified
        val daysBefore = context.getSharedPreferences("preferences", Context.MODE_PRIVATE)
            ?.getInt("daysBefore", 3) ?: 0

        //gets the delay for the notification
        val scheduleDay = Days.daysBetween(LocalDate.now(), nextSub.dueDate).days - daysBefore

        //builds the notification worker
        val myNotification = OneTimeWorkRequestBuilder<NotificationWorker>()
            .setInitialDelay(scheduleDay.toLong(), TimeUnit.DAYS)
            .addTag(date)
            .setInputData(inputData)
            .build()


        //enqueues the work to be done by the workmanager
        WorkManager.getInstance(context)
            .enqueueUniqueWork(
                "notifySubscription",
                ExistingWorkPolicy.REPLACE,
                myNotification
            )
    }

    /**
     * We need to do this for android devices above OS Oreo
     */
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            val name = "Subscription Channel"
            val description = "This channel sends reminders about subscriptions"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val subChannel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                this.description = description
            }

            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(subChannel)
        }

    }

}