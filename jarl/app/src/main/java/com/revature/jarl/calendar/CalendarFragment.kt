package com.revature.jarl.calendar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.revature.jarl.bindSubscriptionRecycler
import com.revature.jarl.databinding.FragmentCalendarBinding
import com.revature.jarl.subscriptions.*
import org.joda.time.LocalDate


class CalendarFragment : Fragment() {

    private lateinit var binding: FragmentCalendarBinding
    private val viewModel: CalenderViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentCalendarBinding.inflate(inflater, container, false)

        binding.calendarViewModel = viewModel
        binding.lifecycleOwner = this

        binding.calenderView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            viewModel.loadSubscriptions(LocalDate(year, month + 1, dayOfMonth))
            viewModel._roomSubscriptions.observe(viewLifecycleOwner, {
                bindSubscriptionRecycler(binding.calendarRecyclerView, it)
            })
        }


        binding.calendarRecyclerView.adapter = SubscriptionsRecyclerAdapter(
            SubscriptionsRecyclerAdapter.CustomSubscriptionClickListener { subscription ->
                findNavController().navigate(
                    CalendarFragmentDirections.actionCalenderFragmentToSubscriptionDetailsFragment(
                        subscription.subscriptionId
                    )
                )
            }
        )

        return binding.root
    }
}