# JARL

Subscription Organization by JARL

## Project Description

This project is designed to help users track subscriptions they have. It will send them reminders when those subscriptions come due.

## Technologies Used

* Kotlin - version 30.0
* Android Studio - version 4.1.3

## Features

List of features:
* Login Functionality
* Offline Notifications
* Saved Subscriptions
* Custom Subscriptions
* Total Monthly Cost Tracker

## Getting Started
   
Git Clone: "git@gitlab.com:alecramirez/jarl.git"

Http Clone: https://gitlab.com/alecramirez/jarl.git

Instructions for Windows:
* Download files
* Open files in Android Studio 4.1.3 or later
* Compile and Run Project



## Usage

This project is designed to assist users in remembering their subscriptions. It does not contain any payment functionality; the program only notifies you of the upcoming subscription. In addition, subscriptions must be added to the program directly; there is no functionality to automatically capture subscriptions.

## Contributors

* Liam Heaney
* Jacob Ginn
* Alec Ramirez
* Roya Askari

## License

This product is licensed by Revature
